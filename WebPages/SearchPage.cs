﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Tests.WebPages
{
    class SearchPage : BaseTestClass
    {
        public static IWebElement NoteText()
        {
            IWebElement element = driver.FindElement(By.ClassName("notetext"));
            return element;
        }
        public static IWebElement SearchField()
        {
            IWebElement element = driver.FindElement(By.Id("searchinpm"));
            return element;
        }
        public static IWebElement SearchButton()
        {
            IWebElement element = driver.FindElement(By.CssSelector("#sform > div.search-button > input[type=submit]"));
            return element;
        }
        public static IList<IWebElement> SearchResults()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.TagName("a.name")));
            IList<IWebElement> elements = driver.FindElements(By.TagName("a.name"));
            return elements;
        }
    }
}
