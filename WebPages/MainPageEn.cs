﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Tests.WebPages
{
    class MainPageEn : BaseTestClass
    {
        public static IList<IWebElement> News()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.TagName("dt")));
            IList<IWebElement> elements = driver.FindElements(By.TagName("dt"));
            return elements;
        }
        public static IWebElement Copyright()
        {
            IWebElement element = driver.FindElement(By.CssSelector("body > div.main-wrappper > div.main-wrapper.index > div.footer-extra > div.copyright"));
            return element;
        }
        public static IList<IWebElement> MenuButtons()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.TagName("td")));
            IList<IWebElement> elements = driver.FindElements(By.TagName("td"));
            return elements;
            
        }
    }
}
