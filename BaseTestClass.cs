﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System;
using System.Linq;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Firefox;

namespace Tests
{   
    [TestFixture]
    public class BaseTestClass
    {
        static public IWebDriver driver = new ChromeDriver();
        static public WebDriverWait wait;
        static public Actions actions;
        static public IWebDriver DriverSetUp(IWebDriver driver)
        {
            BaseTestClass.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            actions = new Actions(driver);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Manage().Window.Maximize();
            return driver;
        }
        private static Random random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static string RandomString(int length)
        {
            string MyRandomString = "";
            for (int i = 0; i < length; i++)
            {
                MyRandomString += chars[random.Next(0, chars.Count() - 1)];
            }
            return MyRandomString;
        }
        static public bool expection;
        static public string mytext;

        [OneTimeTearDown]
        static public void CloseBrowser()
        {
            driver.Quit();
        }
    }
}


