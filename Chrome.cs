﻿using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using Tests.WebPages;
using System.Threading;

namespace Tests
{
    [TestFixture]
    [Parallelizable]
    public class Chrome : BaseTestClass
    {
        [OneTimeSetUp]
        static public void SetUp()
        {
            driver = DriverSetUp(driver);

        }

        [Test]
        static public void GoogleVisit()
        {
            driver.Url = "https://www.google.com/";
            Google.SearchField().SendKeys("Белорусская железная дорога" + Keys.Enter);
            Google.RailwayLink().Click();
            wait.Until(ExpectedConditions.UrlToBe("https://www.rw.by/be/"));
        }

        [Test]
        static public void MenuButtonsCheck()
        {
            driver.Url = "https://www.rw.by/ru/";
            MainPageRu.Language().Click();
            MainPageRu.English().Click();
            Assert.IsTrue(MainPageEn.News().Count >= 4);
            foreach (IWebElement e in MainPageEn.News())
            {
                Assert.IsTrue(e.Displayed);
            }
            Assert.IsTrue(MainPageEn.Copyright().Displayed);
            Assert.IsTrue(MainPageEn.MenuButtons().Count == 5);
            List<string> ButtonsNames = new List<string>() { "TIMETABLE", "FREIGHT", "PASSENGER SERVICES", "CORPORATE", "PRESS CENTER" };
            foreach (IWebElement e  in MainPageEn.MenuButtons())
            {
                foreach (string s in ButtonsNames)
                {
                    if (e.Text == s)
                    {
                        Assert.IsTrue(e.Displayed);
                        expection = true;
                    }
                }
                Assert.AreEqual(expection, true);
                expection = false;
            }
        }

        [Test]
        static public void SearchCheck()
        {
            driver.Url = "https://www.rw.by/ru/";
            mytext = RandomString(20);
            MainPageRu.SearchField().SendKeys(mytext + Keys.Enter);
            wait.Until(ExpectedConditions.UrlToBe("https://www.rw.by/search/?s=Y&q=" + mytext));
            Assert.AreEqual("https://www.rw.by/search/?s=Y&q=" + mytext, driver.Url);  
            Assert.IsTrue(SearchPage.NoteText().Displayed);
            SearchPage.SearchField().Clear();
            SearchPage.SearchField().SendKeys("Санкт-Петербург");
            SearchPage.SearchButton().Click();
            Assert.IsTrue(SearchPage.SearchResults().Count == 15);
            foreach (IWebElement e in SearchPage.SearchResults())
            {
                Console.WriteLine(e.Text);
                Assert.IsTrue(e.Displayed);
            }
        }

        [Test] 
        static public void TrainsSearch()
        {
            driver.Url = "https://www.rw.by/ru/";
            MainPageRu.FromWhereField().SendKeys("Брест");
            actions.SendKeys(MainPageRu.ToWhereField(), "Минск").Build().Perform();
            MainPageRu.WhenField().Click();
            MainPageRu.Dates()[5].Click();
            MainPageRu.SearchButton().Click();
            for (int i = 0; i < TimeTable.Trains().Count; i++)
            {
                Console.WriteLine($"{TimeTable.Trains()[i].Text} - {TimeTable.StartTime()[i].Text}");
            }
            TimeTable.Trains()[0].Click();
            Assert.IsTrue(TimeTable.TrainTitle().Displayed);
            Assert.IsNotEmpty(TimeTable.CalendarDescription(). Text);
            TimeTable.Logo().Click();
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            wait.Until(ExpectedConditions.UrlToBe("https://www.rw.by/"));
            Assert.AreEqual("https://www.rw.by/", driver.Url);
        }
    }
}

